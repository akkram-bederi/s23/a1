
//#3 insertOne
db.users.insertOne({"name":"single","accomodates":2,"price":1000,
					"description":"A simple room with all the basic necessities",
					"rooms_available":10, "isAvailable":false})
//#4 insertMany
db.users.insertMany([
					{"name":"queen","accomodates":4,"price":4000,
					"description":"A room with a queen sized bed perfect for a simple getaway",
					"rooms_available":15, "isAvailable":false},
					{"name":"queen","accomodates":4,"price":4000,
					"description":"A room with a queen sized bed perfect for a simple getaway",
					"rooms_available":15, "isAvailable":false},
					{"name":"queen","accomodates":4,"price":4000,
					"description":"A room with a queen sized bed perfect for a simple getaway",
					"rooms_available":15, "isAvailable":false}])
					
//#5 find
db.users.find({"name":"double"})

//#6 updateOne

db.users.updateOne({"name":"queen"},{$set:{"rooms_available":0}})

//#7

db.users.deleteMany({"rooms_available":0})
